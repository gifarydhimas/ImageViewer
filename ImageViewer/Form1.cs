﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageViewer
{
    public partial class Form1 : Form
    {
        OpenFileDialog ofd = new OpenFileDialog();
        SaveFileDialog sfd = new SaveFileDialog();
        private int activatedIdx;
        private List<PictureForm> pictureForms = new List<PictureForm>();
        private List<ToolStripMenuItem> menus = new List<ToolStripMenuItem>();
        public int allDegree = 0;

        public Form1()
        {
            InitializeComponent();
            //this.imageSource = imgSrc;
            //origImg = Image.FromFile(imgSrc);
            //pictureBox_1063.Image = origImg;
            sfd.Filter = "JPEG Image|*.jpg";
        }

        public void toolStripButton(object sender, EventArgs e)
        {

        }

        public void CloseForm(PictureForm form)
        {
            for(int i = 0; i < pictureForms.Count; i++)
            {
                PictureForm f = pictureForms[i];
                if (f.Equals(form))
                {
                    windowToolStripMenuItem.DropDownItems.RemoveAt(i);

                    pictureForms.RemoveAt(i);
                    menus.RemoveAt(i);
                    return;
                }
            }
        }

        private void RefreshMenuItem()
        {
            for(int i = 0; i < menus.Count; i++)
            {
                if (i != activatedIdx)
                {
                    menus[i].Checked = false;
                } else
                {
                    menus[i].Checked = true;
                }
            }
        }

        public void ActivateForm(PictureForm form)
        {
            for (int i = 0; i < pictureForms.Count; i++)
            {
                PictureForm f = pictureForms[i];
                if (f.Equals(form))
                {
                    activatedIdx = i;
                    RefreshMenuItem();
                }
            }
        }

        private void MenuItemClickHandler(object sender, EventArgs e)
        {
            for (int i = 0; i < menus.Count; i++)
            {
                ToolStripMenuItem menu = menus[i];
                if (sender.Equals(menu))
                {
                    pictureForms[i].Activate();
                    activatedIdx = i;
                }
            }
            
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            //Console.WriteLine("sender: {0}\neventArgs: {1}\n", sender, e);
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var form = new PictureForm(ofd.FileName, this);
                form.MdiParent = this;
                form.Show();
                pictureForms.Add(form);

                var tsItem = new ToolStripMenuItem(ofd.SafeFileName, null, MenuItemClickHandler);
                tsItem.Checked = true;
                windowToolStripMenuItem.DropDownItems.Add(tsItem);
                menus.Add(tsItem);

                activatedIdx = menus.Count - 1;
                RefreshMenuItem();

                //pictureBox_1063.Image = Image.FromFile(ofd.FileName);
                //origImg = pictureBox_1063.Image;
            }
        }

        public void SetZoomAll(int w, int h)
        {
            foreach(PictureForm f in pictureForms)
            {
                f.SetZoom(w, h);
            }
        }

        private void zoomInStripButton_Click(object sender, EventArgs e)
        {
            pictureForms[activatedIdx].ZoomIn();
        }

        private void zoomOutStripButton_Click(object sender, EventArgs e)
        {
            pictureForms[activatedIdx].ZoomOut();
        }

        private void rotateLeftStripButton_Click(object sender, EventArgs e)
        {
            pictureForms[activatedIdx].RotateLeft();
        }

        private void rotateRightStripButton_Click(object sender, EventArgs e)
        {
            pictureForms[activatedIdx].RotateRight();
        }

        private void scrollHandler(object sender, MouseEventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void saveToolStripButton_1063_Click(object sender, EventArgs e)
        {
            //if (sfd.ShowDialog() == DialogResult.OK)
            //{
            //    // save as jpeg
            //    pictureBox_1063.Image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            //}
        }

        private void printToolStripButton_1063_Click(object sender, EventArgs e)
        {

            
        }


    }
}
