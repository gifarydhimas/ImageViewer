﻿namespace ImageViewer
{
    partial class PictureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.tbDegree = new System.Windows.Forms.TextBox();
            this.btnSetAllDegree = new System.Windows.Forms.Button();
            this.btnApplyDegree = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 371);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(3, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(353, 194);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // tbDegree
            // 
            this.tbDegree.Location = new System.Drawing.Point(34, 399);
            this.tbDegree.Name = "tbDegree";
            this.tbDegree.Size = new System.Drawing.Size(100, 22);
            this.tbDegree.TabIndex = 1;
            // 
            // btnSetAllDegree
            // 
            this.btnSetAllDegree.Location = new System.Drawing.Point(153, 387);
            this.btnSetAllDegree.Name = "btnSetAllDegree";
            this.btnSetAllDegree.Size = new System.Drawing.Size(127, 51);
            this.btnSetAllDegree.TabIndex = 2;
            this.btnSetAllDegree.Text = "Set Degree All";
            this.btnSetAllDegree.UseVisualStyleBackColor = true;
            this.btnSetAllDegree.Click += new System.EventHandler(this.btnSetAllDegree_Click);
            // 
            // btnApplyDegree
            // 
            this.btnApplyDegree.Location = new System.Drawing.Point(286, 387);
            this.btnApplyDegree.Name = "btnApplyDegree";
            this.btnApplyDegree.Size = new System.Drawing.Size(127, 51);
            this.btnApplyDegree.TabIndex = 2;
            this.btnApplyDegree.Text = "Apply Degree";
            this.btnApplyDegree.UseVisualStyleBackColor = true;
            this.btnApplyDegree.Click += new System.EventHandler(this.btnApplyDegree_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(419, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 51);
            this.button1.TabIndex = 2;
            this.button1.Text = "Zoom In All";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(552, 387);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 51);
            this.button2.TabIndex = 2;
            this.button2.Text = "Zoom Out All";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PictureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnApplyDegree);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSetAllDegree);
            this.Controls.Add(this.tbDegree);
            this.Controls.Add(this.panel1);
            this.Name = "PictureForm";
            this.Text = "PictureForm";
            this.Activated += new System.EventHandler(this.PictureForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PictureForm_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TextBox tbDegree;
        private System.Windows.Forms.Button btnSetAllDegree;
        private System.Windows.Forms.Button btnApplyDegree;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}