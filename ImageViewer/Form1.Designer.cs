﻿namespace ImageViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip_1063 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomInStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.zoomOutStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.rotateLeftStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.rotateRightStripButton_1063 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip_1063.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip_1063
            // 
            this.toolStrip_1063.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip_1063.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton_1063,
            this.saveToolStripButton_1063,
            this.printToolStripButton_1063,
            this.toolStripSeparator,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator1,
            this.helpToolStripButton,
            this.toolStripSeparator2,
            this.zoomInStripButton_1063,
            this.zoomOutStripButton_1063,
            this.rotateLeftStripButton_1063,
            this.rotateRightStripButton_1063});
            this.toolStrip_1063.Location = new System.Drawing.Point(0, 28);
            this.toolStrip_1063.Name = "toolStrip_1063";
            this.toolStrip_1063.Size = new System.Drawing.Size(800, 27);
            this.toolStrip_1063.TabIndex = 0;
            this.toolStrip_1063.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.newToolStripButton.Text = "&New";
            // 
            // openToolStripButton_1063
            // 
            this.openToolStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton_1063.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton_1063.Image")));
            this.openToolStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton_1063.Name = "openToolStripButton_1063";
            this.openToolStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.openToolStripButton_1063.Text = "&Open";
            this.openToolStripButton_1063.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton_1063
            // 
            this.saveToolStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton_1063.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton_1063.Image")));
            this.saveToolStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton_1063.Name = "saveToolStripButton_1063";
            this.saveToolStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.saveToolStripButton_1063.Text = "&Save";
            this.saveToolStripButton_1063.Click += new System.EventHandler(this.saveToolStripButton_1063_Click);
            // 
            // printToolStripButton_1063
            // 
            this.printToolStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton_1063.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton_1063.Image")));
            this.printToolStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton_1063.Name = "printToolStripButton_1063";
            this.printToolStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.printToolStripButton_1063.Text = "&Print";
            this.printToolStripButton_1063.Click += new System.EventHandler(this.printToolStripButton_1063_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // cutToolStripButton
            // 
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripButton.Image")));
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.cutToolStripButton.Text = "C&ut";
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.copyToolStripButton.Text = "&Copy";
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.pasteToolStripButton.Text = "&Paste";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(24, 24);
            this.helpToolStripButton.Text = "He&lp";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // zoomInStripButton_1063
            // 
            this.zoomInStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomInStripButton_1063.Image = global::ImageViewer.Properties.Resources.icons8_zoom_in_32;
            this.zoomInStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomInStripButton_1063.Name = "zoomInStripButton_1063";
            this.zoomInStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.zoomInStripButton_1063.Text = "Zoom In";
            this.zoomInStripButton_1063.Click += new System.EventHandler(this.zoomInStripButton_Click);
            // 
            // zoomOutStripButton_1063
            // 
            this.zoomOutStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomOutStripButton_1063.Image = global::ImageViewer.Properties.Resources.icons8_zoom_out_32;
            this.zoomOutStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomOutStripButton_1063.Name = "zoomOutStripButton_1063";
            this.zoomOutStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.zoomOutStripButton_1063.Text = "Zoom Out";
            this.zoomOutStripButton_1063.Click += new System.EventHandler(this.zoomOutStripButton_Click);
            // 
            // rotateLeftStripButton_1063
            // 
            this.rotateLeftStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rotateLeftStripButton_1063.Image = global::ImageViewer.Properties.Resources.icons8_rotate_left_32;
            this.rotateLeftStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rotateLeftStripButton_1063.Name = "rotateLeftStripButton_1063";
            this.rotateLeftStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.rotateLeftStripButton_1063.Text = "Rotate Left";
            this.rotateLeftStripButton_1063.Click += new System.EventHandler(this.rotateLeftStripButton_Click);
            // 
            // rotateRightStripButton_1063
            // 
            this.rotateRightStripButton_1063.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rotateRightStripButton_1063.Image = global::ImageViewer.Properties.Resources.icons8_rotate_right_32;
            this.rotateRightStripButton_1063.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rotateRightStripButton_1063.Name = "rotateRightStripButton_1063";
            this.rotateRightStripButton_1063.Size = new System.Drawing.Size(24, 24);
            this.rotateRightStripButton_1063.Text = "Rotate Right";
            this.rotateRightStripButton_1063.Click += new System.EventHandler(this.rotateRightStripButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.windowToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.windowToolStripMenuItem.Text = "Window";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip_1063);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Image Viewer";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.toolStrip_1063.ResumeLayout(false);
            this.toolStrip_1063.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_1063;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton_1063;
        private System.Windows.Forms.ToolStripButton saveToolStripButton_1063;
        private System.Windows.Forms.ToolStripButton printToolStripButton_1063;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton zoomInStripButton_1063;
        private System.Windows.Forms.ToolStripButton zoomOutStripButton_1063;
        private System.Windows.Forms.ToolStripButton rotateLeftStripButton_1063;
        private System.Windows.Forms.ToolStripButton rotateRightStripButton_1063;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
    }
}

