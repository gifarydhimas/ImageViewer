﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageViewer
{
    public partial class PictureForm : Form
    {

        private int deg = 0;
        private Image origImg;
        private Form1 parent;

        public PictureForm(string src, Form1 parent)
        {
            InitializeComponent();
            origImg = Image.FromFile(src);
            this.pictureBox.Image = origImg;
            this.parent = parent;
            this.Text = System.IO.Path.GetFileName(src);
            UpdateTextBoxDegree();
        }

        public void SetZoom(int w, int h)
        {
            pictureBox.Width = w;
            pictureBox.Height = h;
        }

        public void ZoomIn()
        {
            pictureBox.Width = pictureBox.Width + 50;
            pictureBox.Height = pictureBox.Height + 50;
        }

        public void ZoomOut()
        {
            if (pictureBox.Width > 50)
                pictureBox.Width = pictureBox.Width - 50;
            if (pictureBox.Height > 50)
                pictureBox.Height = pictureBox.Height - 50;
        }

        public void RotateLeft()
        {
            Image flipImage = pictureBox.Image;
            flipImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
            if (deg < 180)
            {
                pictureBox.Image = origImg;
                deg = 0;
                return;
            }
            pictureBox.Image = flipImage;
            deg -= 90;

            UpdateTextBoxDegree();
        }

        public void RotateRight()
        {
            Image flipImage = pictureBox.Image;
            flipImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
            if (deg > 270)
            {
                pictureBox.Image = origImg;
                deg = 0;
                return;
            }
            deg += 90;
            pictureBox.Image = flipImage;

            UpdateTextBoxDegree();
        }

        private void UpdateTextBoxDegree()
        {
            tbDegree.Text = deg.ToString();
        }

        private void PictureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.CloseForm(this);
        }

        private void PictureForm_Activated(object sender, EventArgs e)
        {
            parent.ActivateForm(this);
        }

        private void btnSetAllDegree_Click(object sender, EventArgs e)
        {
            parent.allDegree = deg;
        }

        private void btnApplyDegree_Click(object sender, EventArgs e)
        {
            int a1, a2;
            a1 = parent.allDegree / 90;
            a2 = deg / 90;
            if (a2 < a1)
            {
                for(int i = 0; i < a1; i++)
                {
                    RotateRight();
                }
            } else if (a2 > a1)
            {
                for(int i = a1; i < a2; i++)
                {
                    RotateLeft();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ZoomIn();
            parent.SetZoomAll(pictureBox.Width, pictureBox.Height);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ZoomOut();
            parent.SetZoomAll(pictureBox.Width, pictureBox.Height);
        }
    }
}
